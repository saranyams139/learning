<?php
/**
 * Template Name: Gallery Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ProTrim_Theme
 */

get_header();
?>

<section style="background-image: url(<?php echo get_framework_directory_uri() ?>/images/banner-gallery.jpg);" class="banner-main inner-banner">
     <?php include(locate_template('template-parts/content-sidelinks.php')); ?>
   </section>

   <?php include(locate_template('template-parts/content-sidepopup.php')); ?>


   <div id="inner-wrapper" class="inner-page gallery">
     <section class="gallery-inner">
       <div class="container">
         <div class="row">

           <div class="col-lg-12 col-md-12 col-12 gall-text text-center">
             <h2 class="stylists-txt wow fadeInUp" data-wow-duration="0.3s" data-wow-delay="0.3s">Gallery</h2>

           </div>

hello
hi123

           <div class="col-lg-12 col-md-12 col-12 gall-name wow fadeInUp" data-wow-duration="0.4s"
             data-wow-delay="0.4s">
             <button class="btn btn-default filter-button active" data-filter="all">Show All</button>
             <button class="btn btn-default filter-button" data-filter="photos">Photos</button>
             <button class="btn btn-default filter-button" data-filter="videos">Videos</button>

           </div>
            <div class="gallery_list row">
            <?php
                $args = array( 
                'post_type' => 'galleries',
                'posts_per_page' => -1,
                'orderby' => 'menu_order',
                'order' => 'DESC'
                );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
                $galimage = get_field('gallery_image');
                if($galimage) {
                $image_id = $galimage['id'];
                $image_alt = $galimage['alt'];
                }
                $fimage = get_field('fancy_image');
                $val = get_field('video_or_image');
                if($val == 'video'){
                ?>
           <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-12 filter videos wow fadeInUp"
             data-wow-duration="0.2s" data-wow-delay="0.2s">
             <a data-fancybox="gallery" href="<?php the_field('video'); ?>">
               <img <?php lazy_responsive_image($image_id,'fhd','1920px'); ?> />
                <button type="button" class="btn gallery-zoom"><i class="fas fa-play"></i></button>
             </a>
           </div>
            <?php
             }
             else
             { ?>
           <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-12 filter photos wow fadeInUp"
             data-wow-duration="0.2s" data-wow-delay="0.2s">
             <a data-fancybox="gallery" href="<?php echo $fimage['url']; ?>">
               <img <?php lazy_responsive_image($image_id,'fhd','1920px'); ?> />
               <button type="button" class="btn gallery-zoom"><i class="fas fa-plus"></i></button>
             </a>
           </div>
           <?php }
              endwhile; 
              wp_reset_postdata(); ?>
            </div>
            <button class="loadmore">View More <i class="fa fa-angle-right"></i></button>
           <!-- <a class="gallbtn wow fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.4s">View More <i
               class="fa fa-angle-right"></i> </a> -->


         </div>
       </div>
     </section>
   </div>

<?php
get_footer();
